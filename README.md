# sameconf - same version constraints for the Python tools and checkers.

The `sameconf` library and command-line tool parse parts of the configuration of
as many Python build tools as checkers as they can, and try to make sure that
the same constraints for a minimum Python version are specified in all of them.

## Command-line usage

The `sameconf` tool can be used to check whether a Python project specifies
the same minimum Python version constraints and the same line length limits in
the configuration files for all the static checkers and build tools:

    sameconf check python [/path/to/project/dir]

It may also report the configuration settings it parsed as a JSON object:

    sameconf show python [/path/to/project/dir]

As an experimental feature, `sameconf` may query PyPI to check if any of
the Python libraries listed as dependencies in the `pyproject.toml` and
`tox.ini` files have released versions that fall outside of the version
constraints in the dependency specifications:

    sameconf check pypi [-s | --skip-prereleases] [/path/to/project/dir]

## Contact

The `sameconf` library was written by [Peter Pentchev][roam];
it is developed in a [GitLab repository][gitlab].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/sameconf "The GitLab sameconf repository"
