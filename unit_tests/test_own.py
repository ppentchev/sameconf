# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Test the sameconf library against its own source code."""

# This is a test suite, right?
# flake8: noqa: S101
# pylint: disable=R2004

import pathlib

from typing import TYPE_CHECKING

import sameconf
import sameconf.defs as sc_defs

if TYPE_CHECKING:
    from typing import Final


PY_OWN_VER = sameconf.PyVer(3, 8)
PY_OWN_LENGTH = 100


def test_own() -> None:
    """Parse the sameconf library's configuration."""
    cfg = sameconf.Config(parser=sameconf.PypParser, path=pathlib.Path(""))
    meta = sameconf.parse_meta(cfg)
    assert meta.pyproject_path.is_file()  # type: ignore[union-attr]
    assert meta.setup_cfg_path.is_file()  # type: ignore[union-attr]
    assert meta.tox_path.is_file()  # type: ignore[union-attr]

    assert meta.pymin == sc_defs.PyMinVersions(
        project=PY_OWN_VER, black=PY_OWN_VER, mypy=PY_OWN_VER, pyupgrade=PY_OWN_VER, ruff=PY_OWN_VER
    )

    assert meta.line_length == sc_defs.PyLineLength(
        black=PY_OWN_LENGTH, flake8=PY_OWN_LENGTH, pycodestyle=PY_OWN_LENGTH, ruff=PY_OWN_LENGTH
    )

    deps_wheel: Final = [
        dep for dep in meta.deps["project.build-system"] if dep.project_name == "wheel"
    ]
    assert len(deps_wheel) == 1, repr(meta.deps["project.build-system"])
    assert not deps_wheel[0].specs, repr(deps_wheel)
    deps_setuptools: Final = [
        dep for dep in meta.deps["project.build-system"] if dep.project_name == "setuptools"
    ]
    assert len(deps_setuptools) == 1, repr(meta.deps["project.build-system"])
    assert len(deps_setuptools[0].specs) == 1, repr(deps_setuptools)
    assert deps_setuptools[0].specs[0][0] == ">=", repr(deps_setuptools)

    deps_req = [
        line.split()[0]
        for line in pathlib.Path("requirements/install.txt")
        .read_text(encoding="UTF-8")
        .splitlines()
        if line
    ]
    for env in (
        "setuptools.dynamic",
        "testenv:mypy",
        "testenv:pylint",
        "testenv:unit-tests",
        "testenv:functional",
    ):
        deps = [dep.project_name for dep in meta.deps[env]]
        assert all(dep in deps for dep in deps_req)

    upper: Final = meta.get_upper_constraints()
    assert "wheel" not in upper
    assert upper["click"] == ("<", sc_defs.PyPkgVer("9"))
