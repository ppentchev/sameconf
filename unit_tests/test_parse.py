# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Parse various combinations of test data files."""

# This is a test suite.
# flake8: noqa: S101

from __future__ import annotations

import pathlib
import shutil
import tempfile

from typing import NamedTuple

import pkg_resources
import pytest

import sameconf

from sameconf import defs as sc_defs


class DataSet(NamedTuple):
    """The filenames of the various files."""

    pyproject: str | None
    setup: str | None
    tox: str | None


PY_VER_GOOD = sc_defs.PyVer(3, 10)
PY_VER_OLD = sc_defs.PyVer(3, 8)

PY_LEN_GOOD = 100
PY_LEN_OLD = 90

PY_REQ_SETUPTOOLS = pkg_resources.Requirement.parse("setuptools >= 61")
PY_REQ_WHEEL = pkg_resources.Requirement.parse("wheel")
PY_REQ_PYLINT = pkg_resources.Requirement.parse("pylint >= 2.14, < 2.16")
PY_REQ_PYPARSING = pkg_resources.Requirement.parse("pyparsing >= 3, < 4")

PY_REQUIREMENTS = [pkg_resources.Requirement.parse("click >= 8, < 9")]

PATH_DATA = pathlib.Path(__file__).parent.parent / "test-data"

PARSE_DATA: list[
    tuple[
        DataSet,
        tuple[
            sc_defs.PyMinVersions,
            sc_defs.PyLineLength,
            dict[str, list[pkg_resources.Requirement]],
        ]
        | type[sc_defs.SameConfError],
    ]
] = [
    (DataSet(None, None, None), (sc_defs.PyMinVersions(), sc_defs.PyLineLength(), {})),
    (
        DataSet("pyproject-empty.toml", "setup-empty.cfg", "tox-empty.ini"),
        (sc_defs.PyMinVersions(), sc_defs.PyLineLength(), {}),
    ),
    (
        DataSet("pyproject-proj-black.toml", "setup-empty.cfg", "tox-empty.ini"),
        (
            sc_defs.PyMinVersions(project=PY_VER_GOOD, black=PY_VER_GOOD),
            sc_defs.PyLineLength(black=PY_LEN_GOOD),
            {
                "project.build-system": [PY_REQ_SETUPTOOLS, PY_REQ_WHEEL],
                "project.dependencies": [PY_REQ_PYPARSING],
            },
        ),
    ),
    (
        DataSet("pyproject-full.toml", "setup-older.cfg", "tox-older.ini"),
        (
            sc_defs.PyMinVersions(
                project=PY_VER_GOOD,
                black=PY_VER_GOOD,
                mypy=PY_VER_GOOD,
                pyupgrade=PY_VER_OLD,
                ruff=PY_VER_GOOD,
            ),
            sc_defs.PyLineLength(
                black=PY_LEN_GOOD, flake8=PY_LEN_OLD, pycodestyle=PY_LEN_OLD, ruff=PY_LEN_GOOD
            ),
            {},
        ),
    ),
    (
        DataSet("pyproject-full.toml", "setup-full.cfg", "tox-full.ini"),
        (
            sc_defs.PyMinVersions(
                project=PY_VER_GOOD,
                black=PY_VER_GOOD,
                mypy=PY_VER_GOOD,
                pyupgrade=PY_VER_GOOD,
                ruff=PY_VER_GOOD,
            ),
            sc_defs.PyLineLength(
                black=PY_LEN_GOOD, flake8=PY_LEN_GOOD, pycodestyle=PY_LEN_GOOD, ruff=PY_LEN_GOOD
            ),
            {"testenv:pylint": PY_REQUIREMENTS + [PY_REQ_PYLINT]},
        ),
    ),
]


@pytest.mark.parametrize(("filenames", "expected"), PARSE_DATA)
def test_parse(
    filenames: DataSet,
    expected: tuple[
        sc_defs.PyMinVersions,
        sc_defs.PyLineLength,
        dict[str, list[pkg_resources.Requirement]],
    ]
    | type[sc_defs.SameConfError],
) -> None:
    """Parse a set of files."""
    with tempfile.TemporaryDirectory("conftest.XXXXXX") as tempd_obj:
        tempd = pathlib.Path(tempd_obj)

        def copy(source: str | None, dest: str) -> None:
            """Copy a file to the temporary directory."""
            if source is not None:
                shutil.copy2(PATH_DATA / source, tempd / dest)

        copy(filenames.pyproject, "pyproject.toml")
        copy(filenames.setup, "setup.cfg")
        copy(filenames.tox, "tox.ini")
        copy("requirements.txt", "requirements.txt")

        cfg = sc_defs.Config(parser=sameconf.PypParser, path=tempd)
        if isinstance(expected, sc_defs.SameConfError):
            with pytest.raises(expected):
                sameconf.parse_meta(cfg)
            return

        assert isinstance(expected, tuple)
        meta = sameconf.parse_meta(cfg)
        assert (meta.pymin, meta.line_length) == (expected[0], expected[1])
        assert {name: [str(item) for item in value] for name, value in meta.deps.items()} == {
            name: [str(item) for item in value] for name, value in expected[2].items()
        }
