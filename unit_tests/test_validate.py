# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Test the validation routines."""

import dataclasses

import pytest

from sameconf import defs as sc_defs
from sameconf import validate as sc_validate

VER_OK = sc_defs.PyVer(3, 10)
VER_OTHER = sc_defs.PyVer(3, 8)
VER_YET_ANOTHER = sc_defs.PyVer(3, 11)

PYMIN_EMPTY = sc_defs.PyMinVersions()
PYMIN_OK = sc_defs.PyMinVersions(project=VER_OK, black=VER_OK, pyupgrade=VER_OK)
PYMIN_MISSING = dataclasses.replace(PYMIN_OK, project=None)


def test_validate_missing() -> None:
    """Expect a "missing version" error."""
    meta = sc_defs.Meta(
        pyproject_path=None,
        setup_cfg_path=None,
        tox_path=None,
        pymin=PYMIN_EMPTY,
        line_length=sc_defs.PyLineLength(),
        deps={},
    )
    with pytest.raises(sc_defs.MissingVersionError):
        sc_validate.validate_meta(meta)

    meta = dataclasses.replace(meta, pymin=PYMIN_MISSING)
    with pytest.raises(sc_defs.MissingVersionError):
        sc_validate.validate_meta(meta)
