import enum

from typing import Any, TypeVar


_S = TypeVar("_S", bound="StrEnum")


class StrEnum(str, enum.Enum):
    def __new__(cls: type[_S], *values: str) -> _S: ...

    def __str__(self) -> str: ...

    @staticmethod
    def _generate_next_value(name: str, start: int, count: int, last_values: list[Any]) -> str: ...
