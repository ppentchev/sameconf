{ pkgs ? (import
    (fetchTarball {
      url = "https://github.com/ppentchev/nixpkgs/archive/2b5194c6b6388d5a828a56172f7cfe092077e84e.tar.gz";
      sha256 = "0i0ginpnhws782gpjil34gzxprbw5g2by1z3ziz4kw3j0nwv6fv7";
    })
    { })
, py-ver ? 311
}:
let
  python-name = "python${toString py-ver}";
  python = builtins.getAttr python-name pkgs;
  python-pkgs = python.withPackages (p: with p; [
    click
    packaging
    pyparsing
    requests
    setuptools

    pytest
  ] ++ pkgs.lib.optionals (python.pythonOlder "3.11") [ backports-strenum ]);
in
pkgs.mkShell {
  buildInputs = [ python-pkgs ];
  shellHook = ''
    set -e
    PYTHONPATH="$(pwd)/src" python3 -m pytest -v unit_tests
    exit
  '';
}
