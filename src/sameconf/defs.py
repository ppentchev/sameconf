# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Common definitions for the sameconf routines."""

from __future__ import annotations

import abc
import dataclasses
import functools
import itertools
import sys

from typing import TYPE_CHECKING, NamedTuple

import packaging.version  # noqa: TCH002
import pkg_resources  # noqa: TCH002

if sys.version_info >= (3, 11):
    import enum as senum
else:
    import backports.strenum as senum

if TYPE_CHECKING:
    from collections.abc import Callable
    from typing import Any, TypeVar

    if sys.version_info >= (3, 10):
        from typing import TypeAlias
    else:
        from typing_extensions import TypeAlias

    import pathlib

    PyTypeAlias: TypeAlias = TypeAlias

    _R = TypeVar("_R", bound="Relation")


VERSION = "0.1.0"

PyPkgVer: TypeAlias = packaging.version.Version
PyPkgReq: TypeAlias = pkg_resources.Requirement


class SameConfError(Exception):
    """The base class for all sameconf errors."""


class ObjectTypeError(SameConfError, ValueError):
    """A file or a value in a file was not of the expected type."""


class MissingVersionError(SameConfError):
    """A version requirement was not specified, but it ought to be."""


class VersionMismatchError(SameConfError):
    """Mismatched Python versions found for some tools."""


class NetworkError(SameConfError):
    """A network error, e.g. could not send an HTTP query."""


class InternalError(SameConfError):
    """Something went terribly, terribly wrong..."""


class PyVer(NamedTuple):
    """A major/minor Python version representation."""

    major: int
    minor: int


@dataclasses.dataclass(frozen=True)
class PyMinVersions:
    """The Python minimal version requirements collected from the configuration."""

    project: PyVer | None = None
    black: PyVer | None = None
    mypy: PyVer | None = None
    pyupgrade: PyVer | None = None
    ruff: PyVer | None = None


@dataclasses.dataclass(frozen=True)
class PyLineLength:
    """The maximum line length specifications collected from the configuration."""

    black: int | None = None
    flake8: int | None = None
    pycodestyle: int | None = None
    ruff: int | None = None


# pylint: disable-next=invalid-enum-extension
class Relation(senum.StrEnum):
    """The relations that interest us."""

    LE = "<="
    LT = "<"

    @classmethod
    def if_interesting(cls: type[_R], rel: str) -> _R | None:
        """Return a relation object if we recognize the string."""
        try:
            return cls(rel)
        except ValueError:
            return None

    def satisfies(self, left: PyPkgVer, right: PyPkgVer) -> bool:
        """Determine whether <left> <rel> <right> holds true."""
        return left < right if self == self.LT else left <= right


@dataclasses.dataclass(frozen=True)
class Meta:
    """A project's metadata gathered from various configuration files."""

    pyproject_path: pathlib.Path | None
    setup_cfg_path: pathlib.Path | None
    tox_path: pathlib.Path | None

    pymin: PyMinVersions
    line_length: PyLineLength
    deps: dict[str, list[PyPkgReq]]

    def get_upper_constraints(self) -> dict[str, tuple[Relation, PyPkgVer]]:
        """Get a name => (relation, version) mapping for upper-constrained dependencies."""
        return dict(
            sorted(
                itertools.chain(
                    *(
                        [
                            (name, (rel, ver))
                            for name, (rel, ver) in (
                                (
                                    dep.project_name,
                                    (Relation.if_interesting(spec[0]), PyPkgVer(spec[1])),
                                )
                                for spec in dep.specs
                            )
                            if rel is not None
                        ]
                        for dep in itertools.chain(*self.deps.values())
                        if not dep.url
                    ),
                )
            )
        )


# pylint: disable-next=invalid-enum-extension
class PyVerFormat(senum.StrEnum):
    """How exactly the Python version is represented."""

    LITERAL = "3.10"
    SQUASHED = "310"
    GE_LITERAL = ">= 3.10"
    PY_SQUASHED = "py310"
    PY_PLUS = "--py310-plus"


class Parser(metaclass=abc.ABCMeta):
    """The base class for the parsing helpers."""

    _data: dict[str, Any]
    _origin: pathlib.Path

    def __init__(self, data: dict[str, Any], origin: pathlib.Path) -> None:
        """Store the data."""
        self._data = data
        self._origin = origin

    @property
    def origin(self) -> pathlib.Path:
        """Get the file the data was read from."""
        return self._origin

    @abc.abstractmethod
    def parse_pyver(self, value: str, path_fmt: str, fmt: PyVerFormat) -> PyVer:
        """Parse a string into a Python version."""
        raise NotImplementedError(f"{type(self).__name__}.parse_pyver() must be overridden")

    @abc.abstractmethod
    def parse_int(self, value: str, path_fmt: str) -> int:
        """Parse a string into an integer."""
        raise NotImplementedError(f"{type(self).__name__}.parse_int() must be overridden")

    def dict_get(self, path: tuple[str, ...]) -> str | list[str] | int | dict[str, Any] | None:
        """Dive into the dictionary, return an element if it is there."""
        path_list = list(path)
        path_fmt = f"[{'.'.join(path_list)}] {self._origin} setting"

        def dive(
            current: int | str | list[str] | dict[str, Any] | None,
            comp: str,
        ) -> str | list[str] | int | dict[str, Any] | None:
            """Dive one step into nested dictionaries if the keys are even present."""
            if current is None:
                return None
            if not isinstance(current, dict):
                raise ObjectTypeError(
                    f"Could not get the '{comp}' component for {path_fmt}: not a table"
                )

            value = current.get(comp)
            if isinstance(value, (type(None), int, str, list, dict)):
                return value

            raise ObjectTypeError(f"Unexpected {path_fmt}: {value!r}")

        # Sigh, it seems that mypy cannot quite fathom unions in some cases
        return functools.reduce(dive, path_list, self._data)  # type: ignore[arg-type]

    def dict_parse_pyver(self, path: tuple[str, ...], fmt: PyVerFormat) -> PyVer | None:
        """Parse a Python version specification if present."""
        path_fmt = f"[{'.'.join(path)}] {self._origin} setting"

        value = self.dict_get(path)
        if value is None:
            return None
        if not isinstance(value, str):
            raise ObjectTypeError(f"Unexpected {path_fmt}: not a string value")
        return self.parse_pyver(value, path_fmt, fmt)

    def dict_parse_pyver_list(
        self,
        path: tuple[str, ...],
        fmt: PyVerFormat,
        *,
        list_handler: Callable[[list[PyVer]], PyVer],
    ) -> PyVer | None:
        """Parse and aggregate a list of Python version specifications if present."""
        path_fmt = f"[{'.'.join(path)}] {self._origin} setting"

        value = self.dict_get(path)
        if value is None:
            return None
        if not isinstance(value, list) or any(not isinstance(item, str) for item in value):
            raise ObjectTypeError(f"Unexpected {path_fmt}: not a list of strings")
        return list_handler([self.parse_pyver(item, path_fmt, fmt) for item in value])

    def dict_parse_int(self, path: tuple[str, ...]) -> int | None:
        """Parse a string value into an integer if present."""
        path_fmt = f"[{'.'.join(path)}] {self._origin} setting"

        value = self.dict_get(path)
        if value is None:
            return None
        if not isinstance(value, str):
            raise ObjectTypeError(f"Unexpected {path_fmt}: not a string value")
        return self.parse_int(value, path_fmt)


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the sameconf routines."""

    parser: type[Parser]
    path: pathlib.Path
