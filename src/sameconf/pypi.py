# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Query the Python package index for available package versions."""

from __future__ import annotations

from typing import TYPE_CHECKING

import requests

from . import defs

if TYPE_CHECKING:
    from typing import Any, Final


ReqSession: defs.PyTypeAlias = requests.Session


def default_headers() -> dict[str, str]:
    """Get the default headers to be sent to the PyPI host."""
    return {"user-agent": f"sameconf/{defs.VERSION}"}


def start_session() -> ReqSession:
    """Start a session that will involve one or more requests.

    This method currently returns a `requests.Session` object. Thus, it may also
    be used as a context manager.
    """
    return ReqSession()


def query_project(sess: ReqSession, name: str) -> dict[str, Any]:
    """Query PyPI for available versions of the specified project."""
    headers: Final = default_headers()
    url: Final = f"https://pypi.org/pypi/{name}/json"
    try:
        resp: Final = sess.get(url, headers=headers)
    except requests.RequestException as err:
        raise defs.NetworkError(
            f"Could not send a GET request to PyPI for {name!r} at {url!r}: {err}"
        ) from err
    try:
        resp.raise_for_status()
    except requests.RequestException as err:
        raise defs.NetworkError(f"Could not query PyPI for {name!r} at {url!r}: {err}") from err

    try:
        res: Final = resp.json()
    except ValueError as err:
        raise defs.ObjectTypeError(
            f"Could not parse the PyPI response for {name!r} at {url!r} as JSON: {err}"
        ) from err
    if not isinstance(res, dict):
        raise defs.ObjectTypeError(
            f"The PyPI JSON response for {name!r} at {url!r} was not an object"
        )
    return res


def check_project_available(
    sess: ReqSession,
    name: str,
    rel: defs.Relation,
    upper_ver: defs.PyPkgVer,
    *,
    skip_prereleases: bool = False,
) -> list[defs.PyPkgVer]:
    """Check for newly-appeared versions of a single project."""
    try:
        avail: Final = query_project(sess, name)
    except defs.SameConfError as err:
        raise defs.NetworkError(f"Could not query PyPI for {name}: {err}") from err

    try:
        versions: Final = sorted(
            ver
            for ver in (defs.PyPkgVer(raw_ver) for raw_ver in avail["releases"])
            if not skip_prereleases or ver >= defs.PyPkgVer(ver.base_version)
        )
    except (KeyError, AttributeError) as err:
        raise defs.NetworkError(f"No 'releases' object in the PyPI response for {name}") from err
    if not versions:
        raise defs.NetworkError(f"No PyPI versions returned for {name}")
    last_ver: Final = versions[-1]
    return [last_ver] if not rel.satisfies(last_ver, upper_ver) else []
