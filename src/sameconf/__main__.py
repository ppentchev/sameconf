# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""sameconf - same version constraints for the Python tools and checkers.

This command-line tool uses the `sameconf` library to analyze the configuration of
various Python build tools and make sure it specifies the same constraints for
a minimum Python version.
"""

from __future__ import annotations

import dataclasses
import functools
import json
import pathlib
import sys

from typing import TYPE_CHECKING

import click
import pkg_resources

from . import defs
from . import parse
from . import pypi
from . import validate

from .parse import pyp

if TYPE_CHECKING:
    from typing import Any, Final


class PathJSONEncoder(json.JSONEncoder):
    """Encode pathlib.Path objects as strings."""

    def default(self, o: Any) -> Any:  # noqa: ANN401
        """Encode pathlib.Path objects as strings, pass anything else through."""
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)

        if isinstance(o, (pathlib.Path, pkg_resources.Requirement)):
            return str(o)

        return super().default(o)


@functools.lru_cache(maxsize=1)
def _json_encoder() -> PathJSONEncoder:
    """Return a singleton instance of our JSON encoder."""
    return PathJSONEncoder(indent=2, sort_keys=True)


@click.command(name="python")
@click.argument("path", type=pathlib.Path, default=".")
def cmd_check_python(path: pathlib.Path) -> None:
    """Check for Python version conflicts."""
    cfg = defs.Config(parser=pyp.Parser, path=path.resolve())
    try:
        meta = parse.parse_meta(cfg)
    except defs.SameConfError as err:
        sys.exit(f"Could not parse the metadata files at {path}: {err}")

    try:
        validate.validate_meta(meta)
    except defs.SameConfError as err:
        sys.exit(f"Validation failed for the metadata files at {path}: {err}")


@click.command(name="pypi")
@click.option("--skip-prereleases", "-s", is_flag=True)
@click.argument("path", type=pathlib.Path, default=".")
def cmd_check_pypi(skip_prereleases: bool, path: pathlib.Path) -> None:  # noqa: FBT001
    """Check for Python libraries with higher versions."""
    cfg = defs.Config(parser=pyp.Parser, path=path.resolve())
    try:
        meta = parse.parse_meta(cfg)
    except defs.SameConfError as err:
        sys.exit(f"Could not parse the metadata files at {path}: {err}")

    try:
        upper: Final = meta.get_upper_constraints()
    except defs.SameConfError as err:
        sys.exit(f"Could not determine the project's upper dependency constraints: {err}")

    retcode = 0
    with pypi.start_session() as sess:
        for name, (rel, upper_ver) in upper.items():
            for last_ver in pypi.check_project_available(
                sess, name, rel, upper_ver, skip_prereleases=skip_prereleases
            ):
                print(f"{name}\t{rel}\t{upper_ver}\t{last_ver}")  # noqa: T201
                retcode = max(retcode, 1)

    sys.exit(retcode)


@click.group(name="check")
def cmd_check() -> None:
    """Check for version conflicts."""


@click.command(name="python")
@click.argument("path", type=pathlib.Path, default=".")
def cmd_show_python(path: pathlib.Path) -> None:
    """Display the configuration settings for Python build and lint tools."""
    cfg = defs.Config(parser=pyp.Parser, path=path.resolve())
    try:
        meta = parse.parse_meta(cfg)
    except defs.SameConfError as err:
        sys.exit(f"Could not parse the metadata files at {path}: {err}")

    print(_json_encoder().encode(meta))  # noqa: T201


@click.group(name="show")
def cmd_show() -> None:
    """Display the parsed configuration settings."""


@click.group(name="sameconf")
def main() -> None:
    """Check for Python version constraint mismatches."""


cmd_check.add_command(cmd_check_python)
cmd_check.add_command(cmd_check_pypi)

cmd_show.add_command(cmd_show_python)

main.add_command(cmd_check)
main.add_command(cmd_show)


if __name__ == "__main__":
    main()
