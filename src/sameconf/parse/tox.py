# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Parse the configuration of some tools from the tox.ini file."""

from __future__ import annotations

import configparser
import dataclasses
import itertools
import pathlib
import shlex

from typing import TYPE_CHECKING

from sameconf import defs

from . import requirements
from . import util

if TYPE_CHECKING:
    from typing import Final


_PROG_PYUPGRADE = "pyupgrade"


def _str_removeprefix(value: str, prefix: str) -> str:
    """Backport Python 3.9's str.removeprefix()."""
    if value.startswith(prefix):
        return value[len(prefix) :]

    return value


def _parse_commands_list(cmd: str) -> list[list[str]]:
    """Parse a Tox `[['foo', 'bar'], [...]]` string."""
    return [shlex.split(line) for line in cmd.splitlines() if line]


def _extract_pyupgrade_command(cmd: list[str]) -> list[str] | None:
    """Look for a `pyupgrade ...` or `sh -c 'pyupgrade ...'` command."""
    # Some versions of pyupgrade don't recursively process directories, so it may be
    # invoked via `sh -c 'pyupgrade foo/*.py bar/baz/*.py'`.
    # The "magic number" 3 will go away at some point when we switch to `match`.
    # pylint: disable-next=R2004
    if len(cmd) == 3 and cmd[0].endswith("sh") and cmd[1] == "-c":  # noqa: PLR2004
        return _extract_pyupgrade_command(shlex.split(cmd[2]))

    if pathlib.Path(cmd[0]).name != _PROG_PYUPGRADE:
        return None

    return cmd


def _extract_pyupgrade_version(parser: defs.Parser, cmd: list[str]) -> defs.PyVer:
    """Get a `PyVer` object out of a pyupgrade invocation."""
    pyopt = [word for word in cmd if word.startswith("--py")]
    if len(pyopt) != 1:
        raise defs.ObjectTypeError(
            f"Expected exactly one `--py*` option in the pyupgrade invocation: {cmd!r}"
        )
    return parser.parse_pyver(pyopt[0], "pyupgrade argument", defs.PyVerFormat.PY_PLUS)


def _find_pyupgrade(
    parser: defs.Parser, env_commands: dict[str, list[list[str]]]
) -> list[defs.PyVer]:
    """Find any pyupgrade commands in all the environments."""
    return sorted(
        {
            _extract_pyupgrade_version(parser, cmd)
            for cmd in (
                _extract_pyupgrade_command(cmd) for cmd in itertools.chain(*env_commands.values())
            )
            if cmd is not None
        }
    )


def _parse_pyupgrade(
    meta: defs.Meta, parser: defs.Parser, env_commands: dict[str, list[list[str]]]
) -> defs.Meta:
    """Find the Python version to which `pyupgrade` is told to conform."""
    pyup_versions: Final = _find_pyupgrade(parser, env_commands)

    if not pyup_versions:
        return meta
    if len(pyup_versions) != 1:
        raise defs.VersionMismatchError(f"Mismatched pyupgrade versions: {pyup_versions}")
    return dataclasses.replace(
        meta, pymin=dataclasses.replace(meta.pymin, pyupgrade=pyup_versions[0])
    )


def _parse_deps(meta: defs.Meta, env_deps: dict[str, list[defs.PyPkgReq]]) -> defs.Meta:
    """Combine the dependencies parsed with those already in the metadata."""
    nonempty: Final[dict[str, list[defs.PyPkgReq]]] = {
        name: deps for name, deps in env_deps.items() if deps
    }
    if not nonempty:
        return meta

    return dataclasses.replace(meta, deps=util.dict_union(meta.deps, nonempty))


def parse_meta_tox_ini(
    cfg: defs.Config, meta: defs.Meta, tox_path: pathlib.Path | None
) -> defs.Meta:
    """Parse tox.ini commands and arguments."""
    if tox_path is None:
        return meta

    parser: Final = cfg.parser({}, tox_path)
    cfgp: Final = configparser.ConfigParser(interpolation=None)
    try:
        cfgp.read_file(tox_path.open(encoding="UTF-8"))
    except FileNotFoundError:
        return meta
    except (OSError, ValueError) as err:
        raise defs.ObjectTypeError(f"Could not read UTF-8 text from {tox_path}: {err}") from err
    except configparser.Error as err:
        raise defs.ObjectTypeError(
            f"Could not parse {tox_path} as an INI-style file: {err}"
        ) from err

    env_commands: Final[dict[str, list[list[str]]]] = {
        parts[2]: _parse_commands_list(section.get("commands", ""))
        for parts, section in (
            (name.partition("testenv:"), section) for name, section in cfgp.items()
        )
        if not parts[0] and parts[1] and parts[2]
    }

    env_deps: Final[dict[str, list[defs.PyPkgReq]]] = {
        name: requirements.parse_requirements_list(section.get("deps", ""), tox_path.parent)
        for name, section in cfgp.items()
        if name.startswith("testenv:")
    }

    return _parse_deps(_parse_pyupgrade(meta, parser, env_commands), env_deps)
