# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Parse the configuration of some tools from the pyproject.toml file."""

from __future__ import annotations

import dataclasses
import sys

from typing import TYPE_CHECKING

from sameconf import defs

from . import requirements
from . import util

if sys.version_info >= (3, 11):
    import tomllib
else:
    import tomli as tomllib

if TYPE_CHECKING:
    import pathlib

    from typing import Any, Final


def _load_toml(path: pathlib.Path) -> Any:  # noqa: ANN401
    """Read a file, parse it as a TOML object."""
    try:
        return tomllib.loads(path.read_text(encoding="UTF_8"))
    except FileNotFoundError:
        return None
    except (OSError, ValueError) as err:
        raise defs.ObjectTypeError(f"Could not parse {path} as valid TOML: {err}") from err


def _parse_build_req(data: list[str] | None) -> list[defs.PyPkgReq] | None:
    """Parse a list of PEP508 requirement specifications."""
    if data is None:
        return None

    try:
        return [defs.PyPkgReq.parse(value) for value in data]
    except ValueError as err:
        raise defs.ObjectTypeError(
            f"Could not parse {data!r} as PEP508 requirements: {err}"
        ) from err


def _parse_meta_deps(
    parser: defs.Parser,
    deps: dict[str, list[defs.PyPkgReq]],
    specs: list[tuple[str, tuple[str, ...]]],
) -> dict[str, list[defs.PyPkgReq]]:
    """Add anything found in this file."""
    updates: dict[str, list[defs.PyPkgReq]] = {}
    for key, spec in specs:
        values = parser.dict_get(spec)
        if values is None:
            continue
        if not isinstance(values, list) or any(not isinstance(item, str) for item in values):
            raise defs.ObjectTypeError(f"Expected a list of strings at [{key}] in {parser.origin}")
        try:
            updates[key] = [defs.PyPkgReq.parse(value) for value in values]
        except ValueError as err:
            raise defs.ObjectTypeError(
                f"Could not parse the [{key}] {parser.origin} dependency {values!r}: {err}"
            ) from err

    return util.dict_union(deps, updates)


def _parse_meta_dyn_deps(
    parser: defs.Parser, deps: dict[str, list[defs.PyPkgReq]], basedir: pathlib.Path
) -> dict[str, list[defs.PyPkgReq]]:
    """Parse setuptools dynamic dependencies from a file."""
    dyn_obj: Final = parser.dict_get(("tool", "setuptools", "dynamic", "dependencies"))
    if dyn_obj is None:
        return deps
    if not isinstance(dyn_obj, dict):
        raise defs.ObjectTypeError(
            f"Expected a table at [tool.setuptools.dynamic.dependencies] in {parser.origin}"
        )

    dyn_file: Final = dyn_obj.get("file")
    if dyn_file is None:
        raise defs.ObjectTypeError(
            f"Unsupported tool.setuptools.dynamic.dependencies value {dyn_obj!r}"
        )
    dyn_path: Final = basedir / dyn_file
    try:
        contents: Final = dyn_path.read_text(encoding="UTF-8")
    except (OSError, ValueError) as err:
        raise defs.ObjectTypeError(
            f"Could not read UTF-8 lines from the dynamic dependencies file {dyn_path}: {err}"
        ) from err
    dyn_deps = requirements.parse_requirements_list(contents, basedir)
    if not dyn_deps:
        return deps
    return util.dict_union(deps, {"setuptools.dynamic": dyn_deps})


def _ruff_get_extended_parser(parser: defs.Parser) -> defs.Parser | None:
    """Get a parser of the same type for the file specified in `[tool.ruff.extend]` if any."""
    ext_name: Final = parser.dict_get(("tool", "ruff", "extend"))
    if ext_name is None:
        return None
    if not isinstance(ext_name, str):
        raise defs.ObjectTypeError(f"Expected a string at [tool.ruff.extend] in {parser.origin}")

    ext_path: Final = parser.origin.parent / ext_name
    proj = _load_toml(ext_path)
    if proj is None:
        raise defs.ObjectTypeError(
            f"Nonexistent {ext_path} specified as {ext_name!r} at "
            f"[tool.ruff.extend] in {parser.origin}"
        )
    return type(parser)(proj, ext_path)


def _ruff_parse_pyver(
    parser: defs.Parser, path: tuple[str, ...], fmt: defs.PyVerFormat
) -> defs.PyVer | None:
    """Parse a `PyVer` object from this file or follow the `[tool.ruff.extend]` chain."""
    value: Final = parser.dict_parse_pyver(path, fmt)
    if value is not None:
        return value

    ext_parser: Final = _ruff_get_extended_parser(parser)
    if ext_parser is None:
        return None
    return _ruff_parse_pyver(ext_parser, path, fmt)


def _ruff_get_int(parser: defs.Parser, path: tuple[str, ...]) -> int | None:
    """Parse a `PyVer` object from this file or follow the `[tool.ruff.extend]` chain."""
    value: Final = parser.dict_get(path)
    if value is not None:
        if not isinstance(value, int):
            raise defs.ObjectTypeError(
                f"Expected an integer value at [{'.'.join(path)}] in {parser.origin}"
            )
        return value

    ext_parser: Final = _ruff_get_extended_parser(parser)
    if ext_parser is None:
        return None
    return _ruff_get_int(ext_parser, path)


def parse_meta_pyproject(
    cfg: defs.Config, meta: defs.Meta, pyproject_path: pathlib.Path | None
) -> defs.Meta:
    """Update the metadata with information read from the pyproject.toml file."""
    if pyproject_path is None:
        return meta

    proj = _load_toml(pyproject_path)
    if proj is None:
        return meta
    if not isinstance(proj, dict):
        raise defs.ObjectTypeError(f"The {pyproject_path} file does not contain a TOML table")

    parser = cfg.parser(proj, pyproject_path)
    return dataclasses.replace(
        meta,
        pymin=dataclasses.replace(
            meta.pymin,
            project=parser.dict_parse_pyver(
                ("project", "requires-python"), defs.PyVerFormat.GE_LITERAL
            ),
            black=parser.dict_parse_pyver_list(
                ("tool", "black", "target-version"),
                defs.PyVerFormat.PY_SQUASHED,
                list_handler=min,
            ),
            mypy=parser.dict_parse_pyver(
                ("tool", "mypy", "python_version"), defs.PyVerFormat.LITERAL
            ),
            ruff=_ruff_parse_pyver(
                parser, ("tool", "ruff", "target-version"), defs.PyVerFormat.PY_SQUASHED
            ),
        ),
        line_length=dataclasses.replace(
            meta.line_length,
            black=parser.dict_get(("tool", "black", "line-length")),
            ruff=_ruff_get_int(parser, ("tool", "ruff", "line-length")),
        ),
        deps=_parse_meta_dyn_deps(
            parser,
            _parse_meta_deps(
                parser,
                meta.deps,
                [
                    ("project.build-system", ("build-system", "requires")),
                    ("project.dependencies", ("project", "dependencies")),
                ],
            ),
            pyproject_path.parent,
        ),
    )
