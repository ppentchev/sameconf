# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Parse the configuration of the Python build tools."""

from __future__ import annotations

from typing import TYPE_CHECKING, NamedTuple

from sameconf import defs

from . import pyproject
from . import setup
from . import tox

if TYPE_CHECKING:
    import pathlib

    from typing import Final


class PathExists(NamedTuple):
    """Store a path and figure out whether it exists on the filesystem."""

    path: pathlib.Path
    exists: bool

    def if_present(self) -> pathlib.Path | None:
        """Return the path if it exists, None otherwise."""
        return self.path if self.exists else None


def _check_path(cfg: defs.Config, filename: str) -> PathExists:
    """Check that a path either does not exist or is a regular file."""
    path: Final = cfg.path / filename
    if path.exists() or path.is_symlink():
        if not path.is_file():
            raise defs.ObjectTypeError(f"{path} is not a regular file")
        return PathExists(path=path, exists=True)

    return PathExists(path=path, exists=False)


def parse_meta(cfg: defs.Config) -> defs.Meta:
    """Parse the configuration of the Python project at the specified path."""
    pyproject_path: Final = _check_path(cfg, "pyproject.toml")
    setup_cfg_path: Final = _check_path(cfg, "setup.cfg")
    tox_path: Final = _check_path(cfg, "tox.ini")

    meta_init: Final = defs.Meta(
        pyproject_path=pyproject_path.if_present(),
        setup_cfg_path=setup_cfg_path.if_present(),
        tox_path=tox_path.if_present(),
        pymin=defs.PyMinVersions(),
        line_length=defs.PyLineLength(),
        deps={},
    )

    return tox.parse_meta_tox_ini(
        cfg,
        setup.parse_meta_setup(
            cfg,
            pyproject.parse_meta_pyproject(cfg, meta_init, pyproject_path.path),
            setup_cfg_path.path,
        ),
        tox_path.path,
    )
