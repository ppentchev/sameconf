# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Parse the configuration of some tools from the setup.cfg file."""

from __future__ import annotations

import configparser
import dataclasses

from typing import TYPE_CHECKING

from sameconf import defs

if TYPE_CHECKING:
    import pathlib


def parse_meta_setup(cfg: defs.Config, meta: defs.Meta, setup_cfg_path: pathlib.Path) -> defs.Meta:
    """Parse configuration out of setup.cfg."""
    cfgp = configparser.ConfigParser(interpolation=None)
    try:
        cfgp.read_file(setup_cfg_path.open(encoding="UTF-8"))
    except FileNotFoundError:
        return meta
    except configparser.Error as err:
        raise defs.ObjectTypeError(f"Could not parse {setup_cfg_path}: {err}") from err

    parser = cfg.parser({name: dict(value) for name, value in cfgp.items()}, setup_cfg_path)
    return dataclasses.replace(
        meta,
        line_length=dataclasses.replace(
            meta.line_length,
            flake8=parser.dict_parse_int(("flake8", "max_line_length")),
            pycodestyle=parser.dict_parse_int(("pycodestyle", "max-line-length")),
        ),
    )
