# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Parse Python dependency requirements."""

from __future__ import annotations

import itertools

from typing import TYPE_CHECKING

from sameconf import defs

if TYPE_CHECKING:
    import pathlib

    from typing import Final


def parse_requirement_line(line: str, basedir: pathlib.Path) -> list[defs.PyPkgReq]:
    """Parse a single requirements line."""
    stripped: Final = line.strip()
    fields: Final = stripped.split()
    if len(fields) == 2 and fields[0] == "-r":  # noqa: PLR2004  # pylint: disable=R2004
        reqfile: Final = basedir / fields[1]
        try:
            contents: Final = reqfile.read_text(encoding="UTF-8")
        except (OSError, ValueError) as err:
            raise defs.ObjectTypeError(
                f"Could not read a UTF-8 requirements list from {reqfile}: {err}"
            ) from err
        return parse_requirements_list(contents, basedir)

    try:
        return [defs.PyPkgReq.parse(stripped)]
    except ValueError as err:
        raise defs.ObjectTypeError(f"Invalid requirements line {line!r}: {err}") from err


def parse_requirements_list(deps_raw: str, basedir: pathlib.Path) -> list[defs.PyPkgReq]:
    """Parse a string consisting of zero or more lines and stuff."""
    return list(
        itertools.chain(
            *(parse_requirement_line(line, basedir) for line in deps_raw.strip().splitlines()),
        )
    )
