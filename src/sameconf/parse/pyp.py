# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Parse Python versions out of various formatted strings."""

from __future__ import annotations

from typing import TYPE_CHECKING, TypeVar

import pyparsing as pyp
import pyparsing.common as pyp_common

from sameconf import defs

if TYPE_CHECKING:
    from typing import Final


_p_ver_literal = (
    pyp_common.integer  # type: ignore[attr-defined]  # pylint: disable=no-member
    + pyp.Literal(".").suppress()
    + pyp_common.integer  # type: ignore[attr-defined]  # pylint: disable=no-member
)

_p_ver_squashed = (
    pyp.Char("0123456789")
    + pyp_common.integer  # type: ignore[attr-defined]  # pylint: disable=no-member
)

_p_ver_py = pyp.Literal("py").suppress() + _p_ver_squashed

_p_req_ge_literal = (
    pyp.Literal(">=").suppress() + pyp.Optional(pyp.White()).suppress() + _p_ver_literal
)

_p_opt_py_plus = pyp.Literal("--").suppress() + _p_ver_py + pyp.Literal("-plus").suppress()


@_p_ver_literal.set_parse_action  # type: ignore[misc]
def _parse_ver_literal(tokens: pyp.ParseResults) -> defs.PyVer:
    """Parse "3.10" into a `PyVer` tuple."""
    res: Final[list[int]] = tokens.as_list()
    return defs.PyVer(res[0], res[1])


@_p_ver_squashed.set_parse_action  # type: ignore[misc]
def _parse_ver_squashed(tokens: pyp.ParseResults) -> defs.PyVer:
    """Parse "310" into a `PyVer` tuple."""
    res: Final[list[str | int]] = tokens.as_list()
    return defs.PyVer(int(res[0]), int(res[1]))


@_p_ver_py.set_parse_action  # type: ignore[misc]
def _parse_ver_py(tokens: pyp.ParseResults) -> defs.PyVer:
    """Parse "py310" into a `PyVer` tuple."""
    res: Final[list[defs.PyVer]] = tokens.as_list()
    return res[0]


@_p_req_ge_literal.set_parse_action  # type: ignore[misc]
def _parse_req_ge_literal(tokens: pyp.ParseResults) -> defs.PyVer:
    """Parse ">= 3.10" into a `PyVer` tuple."""
    res: Final[list[defs.PyVer]] = tokens.as_list()
    return res[0]


@_p_opt_py_plus.set_parse_action  # type: ignore[misc]
def _parse_opt_py_plus(tokens: pyp.ParseResults) -> defs.PyVer:
    """Parse "--py310-plus" into a `PyVer` tuple."""
    res: Final[list[defs.PyVer]] = tokens.as_list()
    return res[0]


p_ver_literal_full = _p_ver_literal.leave_whitespace()

p_ver_squashed_full = _p_ver_squashed.leave_whitespace()

p_ver_py_full = _p_ver_py.leave_whitespace()

p_req_ge_literal_full = _p_req_ge_literal.leave_whitespace()

p_opt_py_plus_full = _p_opt_py_plus.leave_whitespace()

# pylint: disable-next=no-member
p_integer_full = pyp_common.integer.leave_whitespace()  # type: ignore[attr-defined]

_PYVER_PARSERS = {
    defs.PyVerFormat.LITERAL: p_ver_literal_full,
    defs.PyVerFormat.SQUASHED: p_ver_squashed_full,
    defs.PyVerFormat.GE_LITERAL: p_req_ge_literal_full,
    defs.PyVerFormat.PY_SQUASHED: p_ver_py_full,
    defs.PyVerFormat.PY_PLUS: p_opt_py_plus_full,
}


# pylint: disable-next=invalid-name
TValue = TypeVar("TValue", defs.PyVer, int)


def _parse_str(value: str, parser: pyp.ParseExpression, path_fmt: str) -> TValue:
    """Call the specified parser, fetch the `PyVer` object from its result."""
    try:
        res = parser.parse_string(value, parse_all=True).as_list()[0]
    except ValueError as err:
        raise defs.ObjectTypeError(f"Could not parse the {path_fmt} value: {err}") from err

    return res  # type: ignore[no-any-return]


class Parser(defs.Parser):
    """Implement a configuration value parser using the pyparsing library."""

    def parse_pyver(self, value: str, path_fmt: str, fmt: defs.PyVerFormat) -> defs.PyVer:
        """Parse a string into a Python version."""
        try:
            parser = _PYVER_PARSERS[fmt]
        except KeyError as err:
            raise defs.InternalError(f"No PyVer pyparsing parser for the '{fmt}' format") from err
        try:
            res = parser.parse_string(value, parse_all=True).as_list()[0]
        except ValueError as err:
            raise defs.ObjectTypeError(
                f"Could not parse the {path_fmt} as an integer: {err}"
            ) from err
        if not isinstance(res, defs.PyVer):
            raise defs.InternalError(f"{parser!r} returned {res!r} for {value!r}")
        return res

    def parse_int(self, value: str, path_fmt: str) -> int:
        """Parse a string into an integer."""
        try:
            res = p_integer_full.parse_string(value, parse_all=True).as_list()[0]
        except ValueError as err:
            raise defs.ObjectTypeError(
                f"Could not parse the {path_fmt} as an integer: {err}"
            ) from err
        if not isinstance(res, int):
            raise defs.InternalError(f"p_integer_full() returned {res!r} for {value!r}")
        return res
