# Copyright (c) Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
"""Check the metadata for consistency."""

from __future__ import annotations

import dataclasses

from typing import cast

from . import defs


def validate_meta(meta: defs.Meta) -> None:
    """Check the metadata for consistency."""
    proj_ver = meta.pymin.project
    if proj_ver is None:
        raise defs.MissingVersionError("No overall project Python required version specified")

    versions = [
        (name, value) for name, value in dataclasses.asdict(meta.pymin).items() if value is not None
    ]
    assert all(isinstance(value, defs.PyVer) for (name, value) in versions)  # noqa: S101
    versions_bad: list[tuple[str, defs.PyVer]] = [
        (name, cast(defs.PyVer, value)) for name, value in versions if value != proj_ver
    ]
    if versions_bad:
        fmt = ", ".join(
            f"{name}: {value}" for (name, value) in (("project", proj_ver), *versions_bad)
        )
        raise defs.VersionMismatchError(f"Mismatched Python version requirements: {fmt}")

    proj_length = meta.line_length.black
    if proj_length is None:
        raise defs.MissingVersionError("No tool.black.line-length specified in pyproject.toml")

    lengths = [
        (name, value)
        for name, value in dataclasses.asdict(meta.line_length).items()
        if value is not None
    ]
    assert all(isinstance(value, int) for (name, value) in lengths)  # noqa: S101
    lengths_bad: list[tuple[str, int]] = [
        (name, cast(int, value)) for name, value in lengths if value != proj_length
    ]
    if lengths_bad:
        fmt = ", ".join(
            f"{name}: {value}" for (name, value) in (("black", proj_length), *lengths_bad)
        )
        raise defs.VersionMismatchError(f"Mismatched max line length specifications: {fmt}")
